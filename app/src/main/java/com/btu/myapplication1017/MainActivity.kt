package com.btu.myapplication1017

import android.location.GnssMeasurement
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://ktorhighsteaks.herokuapp.com/")
                .build()

        val service = retrofit.create(AnimalService::class.java)



        button.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                val result = service.getResult()
                val barcode = service.postBarCode()
                textView.text = result.animal
                Glide.with(downloadImageView).load(result.photo).into(downloadImageView)
                textView2.text = barcode.name
            }
        }

    }

}

interface AnimalService{
    @GET("animalfacts")
    suspend fun getResult(): AnimalFacts

    @POST("GetByBarcode")
    suspend fun postBarCode():RandomData
}


class AnimalFacts(
        val animal:String,
        val photo:String
)

class RandomData(
        val barcode:String,
        val name: String,
        val price:String,
        val measurement: String
)